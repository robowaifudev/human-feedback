import torch
from torch import nn
import torch.nn.functional as F

class RewardModel(nn.Module):
    '''Human feedback reward model'''
    def __init__(self, in_features):
        super(RewardModel, self).__init__()
        self.fc = nn.Linear(in_features, 1)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return eps * std + mu
        
    def forward(self, features, labels):
        # prediction
        prediction = self.fc(features)
        
        # triangle mask to grab comparisons between rating labels
        rating_mask = torch.ones(prediction.size(0), prediction.size(0), device=prediction.device).tril(-1) > 0
        
        # get the difference between ratings to determine if j > k or k > j
        rating = labels.float().unsqueeze(1)
        rating_diff = (rating - rating.transpose(0, 1))[rating_mask]
        
        j_mask = rating_diff > 0 # j > k
        k_mask = rating_diff < 0 # k > j
        
        # difference between predictions
        prediction_d = (prediction - prediction.transpose(0, 1))[rating_mask]
        
        j = (prediction_d[j_mask]).sigmoid().log()  # j > k losses
        k = (-prediction_d[k_mask]).sigmoid().log() # k > j losses
        
        loss = -torch.concat([j, k])
        return loss

if __name__ == "__main__":
    batch = 8
    features = 1024

    model = RewardModel(features)
    
    x = torch.rand(batch, features)
    labels = torch.arange(batch)

    loss = model(x, labels)
    loss.mean().backward()
    
    print(loss)
