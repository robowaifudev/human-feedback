# human-feedback

Training models with human feedback

## Description
Work in progress on implementing "[Learning to Summarize with Human Feedback](https://openai.com/blog/learning-to-summarize-with-human-feedback/)."

## Preliminary results

![Scatter plot of results](scatter.jpg)

A minibatch from training a reward model for a few hours on modeling image ratings from EfficientNet-B0 features of artwork with heavy data augmentations, using a labelled dataset of 15,000+ images.

## Usage
```python
import torch
from model import RewardModel

batch = 8
features = 1024

model = RewardModel(features)

x = torch.rand(batch, features)
labels = torch.arange(batch)

loss = model(x, labels)
loss.mean().backward()
```

## Roadmap
* Add software for creating human feedback datasets of images and text
* Add improving CycleGAN training with a reward model loss
* Add finetuning GPT-2 with PPO

## License
MIT

## Project status
On hold but will continue development in the future and release existing code as it's cleaned up and ready.
